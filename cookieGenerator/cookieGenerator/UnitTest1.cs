﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.IO;

namespace cookieGenerator
{
    [TestClass]
    public class UnitTest1
    {
        public static IWebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(50));

        public void clickonelement(string selector)
        {
            driver.FindElement(By.CssSelector(selector)).Click();
        }

        public void waitfor(string selector)
        {
            wait.Until(driver => driver.FindElement(By.CssSelector(selector)).Displayed);
        }

        public string randomDate()
        {

            Random rnd = new Random();
            int day = rnd.Next(1, 30);

            string date = day.ToString() + "/" + 11 + "/2017";

            return date;
        }

        public int randomTime(bool hours)
        {
            Random rnd = new Random();
            if(hours == true)
            {
                int hour = rnd.Next(12, 22);
                return hour;
            }
            else
            {
                int minute = rnd.Next(3, 61);
                return minute;
            }
        }

        public void send(string selector, string info)
        {
            driver.FindElement(By.CssSelector(selector)).SendKeys(info);
        }

        public void fillSurvey()
        {
            clickonelement("#opt_8");
            clickonelement("#opt_49797");
            clickonelement("#opt_49809");
            clickonelement("#opt_49820");
            clickonelement("#opt_49831");
            clickonelement("#opt_49842");
            clickonelement("#opt_49853");
            clickonelement("#opt_49864");
            clickonelement("#page_2 > div.new_BtnContent2 > a:nth-child(1) > div");
            waitfor("#question_10124 > div > div > label:nth-child(4)");
            clickonelement("#question_10124 > div > div > label:nth-child(4)");
            clickonelement("#question_10126 > div > div > label:nth-child(4)");
            clickonelement("#\\31 0128 > option:nth-child(23)"); //change here
            clickonelement("#\\31 0129 > option:nth-child(13)"); //change here
            clickonelement("#page_3 > div.new_BtnContent2 > a:nth-child(1) > div");
        }

        [TestMethod]
        public void Generator()
        {
            driver.Navigate().GoToUrl("https://emailfake.com/");

            waitfor("#email_ch_text");

            string email = driver.FindElement(By.CssSelector("#email_ch_text")).Text;

            driver.Navigate().GoToUrl("https://www.tellsubway.ie/ContentManager/Controller.aspx?page=Home/Home");

            send("#txtSearch", "47126");

            clickonelement("#SubmitText");

            waitfor("#agree");

            clickonelement("#agree");

            waitfor("#date_10102");

            send("#date_10102", randomDate());

            clickonelement("#hour_10102 > option:nth-child("+randomTime(true).ToString()+")");
            clickonelement("#minute_10102 > option:nth-child("+randomTime(false).ToString()+")");

            clickonelement("#page_1 > div.new_BtnContent1 > a > div");

            fillSurvey();

            waitfor("#\\31 0130");

            send("#\\31 0130", "email"); //change here

            clickonelement("#question_10131 > div > div > label:nth-child(4)");

            clickonelement("#question_contact_me > div > div > label:nth-child(4)");

            clickonelement("#page_4 > div.new_BtnContent2 > a:nth-child(1) > div");
        }
    }
}
